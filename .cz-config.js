module.exports = {
    types: [
        { value: 'feat', name: 'feat 🍄:    新增新的特性' },
        { value: 'fix', name: 'fix 🐛:    修复 BUG' },
        { value: 'docs', name: 'docs 📄:    修改文档、注释' },
        {
            value: 'refactor',
            name: 'refactor 🎸:    代码重构，注意和特性、修复区分开',
        },
        { value: 'perf', name: 'perf ⚡:    提升性能' },
        { value: 'test', name: 'test 👀:    添加一个测试' },
        { value: 'tool', name: 'tool 🚗:    开发工具变动(构建、脚手架工具等)' },
        { value: 'style', name: 'style ✂:    对代码格式的修改不影响逻辑' },
        { value: 'revert', name: 'revert 🌝:     版本回滚' },
        { value: 'update', name: 'update ⬆:    第三方库升级 ' },
    ],
    ticketNumberPrefix: 'ISSUES:',
    footerPrefix: 'ISSUES:',
    messages: {
        type: '选择一种你的提交类型:',
        subject: '简要说明:\n',
        body: '详细说明，使用"|"换行 (可选)：\n',
        breaking: '非兼容性说明 (可选):\n',
        footer: '关联的issue (可选):\n',
        confirmCommit: '确定提交?',
    },
    subjectLimit: 100,
    skipQuestions: ['scope'],
};
