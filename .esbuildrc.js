// @ts-check
const PluginPostCSS = require('./lib').default
const { build } = require('esbuild')
/**
 * @type { import('esbuild').BuildOptions }
 */
let config = {
    sourcemap: 'external',
    entryPoints: ['test/index.tsx'],
    external: ['react', 'react-dom'],
    outfile: 'output/bundle.js',
    target: 'chrome70',
    jsxFactory: 'React.createElement',
    bundle: true,
    format: 'iife',
    plugins: [
        PluginPostCSS({
            // 根据less文件生成.d.ts文件 (from v0.1.1)
            declaration: true,
        })
    ],
    loader: {
        '.tsx': 'tsx',
        '.ts': 'ts',
    },
    tsconfig: './tsconfig.json',
};

build(config)
