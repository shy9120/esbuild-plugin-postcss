# esbuild-plugin-postcss

#### 介绍
esbuild的postcss插件用于支持ts模块中支持引入css

#### 使用说明

`npm i esbuild-plugin-postcss --save-dev`

.esbuildrc.js
```js
const PluginPostCSS = require('esbuild-plugin-postcss').default
esbuild({
    ...
    plugins: [
        PluginPostCSS({
            // 根据less文件生成.d.ts文件 (from v0.1.1)
            declaration: true,
        })
    ],
    loader: {
        '.tsx': 'tsx',
        '.ts': 'ts',
    },
})
```

index.less
```less
.index {
    background-color: red;
    font: 500 24px/48px "Microsoft Yahei";
    .tab {
        background-color: black;
        color: bisque;
    }
}
```

App.tsx
```jsx
import style from "./style.less"

const App = () => {
    return <div className={style.index}>
        <div>
            <span>标题</span>
        </div>
        <div className={style.tab}>tab1</div>
        <div className={style.tab}>tab2</div>
        <div className={style.tab}>tab3</div>
    </div>
}
```

> PS: typescript 支持 `.less`资源可能需要配置全局module
```ts   
declare module "*.less" {
    const content: Record<string, string>;
    export default content;
}
```
