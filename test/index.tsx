import * as React from 'react'
import * as ReactDOM from 'react-dom'
import style from "./style.less"
import RedTab from './tab'

const App = () => {
    return <div className={style.index}>
        <div>
            <span>标题</span>
        </div>
        <div className={style.tab}>tab1</div>
        <div className={style.tab}>tab2</div>
        <RedTab />
        <div className={style.tab}>tab3</div>
    </div>
}

ReactDOM.render(<App/>, document.getElementById('app'))